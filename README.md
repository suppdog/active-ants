# Active Ants API client


This package provides a wrapper for the ActiveAnts ShopApi. This package was developed
by Suppdog to make a reliable connection between Suppdog (Retail Software) and Active Ants
and provides all the basic functionality.


## Installation

To install, use composer:

```
composer require suppdog/active-ants
```

## Usage

First get an account at ActiveAnts and obtain a username and password for the 
ShopApi.

Start the app with the following code. The application will obtain an authorization-token, 
retreive settings and cache these in the cache folder.

```php
App::start($endpoint, $username, $password, $cacheDirectory);
```

Below you'll find a subset of the available methods.


### Create a product

```php
$product = Product::model()
        ->setName('testProduct')
        ->setSku('testSku');

if ($product->save()) {
    echo "Product was saved";
}
```


### Create an order

```php
$item = OrderItem::model()
        ->setSku('testSku')
        ->setGrossPrice(1.21)
        ->setName('Test Product')
        ->setTaxRate(21);

$address = Address::model()
        ->setName('Suppdog')
        ->setAddress('Street', 1, 'a')
        ->setCity('Stad')
        ->setCountry('NL')
        ->setPostalcode('0000AB');

$order = Order::model()
        ->setEmail('support@wraft.nl')
        ->setOrderId('#1')
        ->setPhoneNumber('0240000000')
        ->addOrderItem($item)
        ->setBillingAddress($address)
        ->setShippingAddress($address);

if ($order->save()) {
    echo "Order was saved";
}
```


### Get stock for all products

```php
foreach (Stock::model()->findAll() as $stock) {
    echo $stock->sku . ': ' . $stock->stock . "\n";
}
```
